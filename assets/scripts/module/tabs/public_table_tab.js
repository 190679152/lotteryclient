cc.Class({
    extends: cc.Component,

    properties: {
        idx: 0,
        labName: cc.Label,
    },

    // use this for initialization
    init (tabInfo) { // sidebar, idx, iconSF
        var info = tabInfo;
        this.node.width = info.sizeX;
        this.sidebar = info.sidebar;
        this.idx = info.idx;
        this.labName.string = info.name;
        this.labName.node.width = info.sizeX;
        this.node.on('touchstart', this.onPressed.bind(this), this.node);
    },

    onPressed () {
        this.sidebar.tabPressed(this.idx,false);
    },

});
