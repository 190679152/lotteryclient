cc.Class({
    extends: cc.Component,

    properties: {
        pfOpenItem:{
            default:null,
            type:cc.Prefab
        },

        pfBaseItem:{
            default:null,
            type:cc.Prefab
        },

        pfBase1Item:{
            default:null,
            type:cc.Prefab
        },

        //开奖界面
        ndOpenPanle:{
            default:null,
            type:cc.Node
        },

        //走势界面
        ndWanPanle:{
            default:null,
            type:cc.Node
        },
        
        ndQianPanle:{
            default:null,
            type:cc.Node
        },
        

        spSort:{
            default:[],
            type:cc.SpriteFrame
        },

        togSort: [cc.Toggle],

        _frontTg:null,
        _data:null,
        _reaCount: 18,
        _spacing: 4,
        _totalCount: 50,
        _openItemArr: [],
        _addType: false,
        _addType1: false
    },

    // use this for initialization
    onLoad: function () {
        this.showPage(this._data);
    },

    showPage:function(info){
      //  cc.log("trend showpage");
        this._color =[];
        var color = [];
        var color1 = new cc.Color(253, 253, 251);
        var color2 = new cc.Color(246, 247, 241);
        color.push(color1);
        color.push(color2);
        this._color.push(color1);
        this._color.push(color2);

        var tempinfo = eval('('+ info +')');
        var trdata = tempinfo["tr"]; 
        
        this._openData = trdata;
  
        //----
        this._totalCount =this._openData.length;
        this.lastPositionY =0;
        this.buffer =800;

        for(var i=0;i<this._reaCount;++i){
            var openItem =cc.instantiate(this.pfOpenItem);
            openItem.setPosition(0,-openItem.height*(i)-this._spacing*(i+1));
            var data = {
                Isuse:this._openData[i].i,
                openNums:this._openData[i].n,
                sum:this._openData[i].s,
                span:this._openData[i].sp,
                repeat:this._openData[i].d,
                color:this._color[i%2],
                type:2
            };
            openItem.getComponent(openItem.name).onItemIndex(i);
            openItem.getComponent(openItem.name).init(data); 
            this.ndOpenPanle.addChild(openItem);
            this._openItemArr.push(openItem);
        }
        this.openItemHeight =openItem.height;
        this.ndOpenPanle.height =this._totalCount*(this.openItemHeight+this._spacing)+this._spacing;

        this.baseItemArr =[];
        this.baseItemArr1 =[];
        this.base1ItemArr =[];
        this.base1ItemArr1 =[];
        for(var i=0;i<this._openData.length;i++)
        {
            //基本走势
            var baseItem = cc.instantiate(this.pfBaseItem);
            var data = {
                Isuse:this._openData[i].i,
                nums:this._openData[i].zw,
                color:color[i%2]
            };
            baseItem.getComponent(baseItem.name).init(data);
            this.baseItemArr.push(baseItem);
           
            //基本走势
            var baseItem = cc.instantiate(this.pfBaseItem);
            var data = {
                Isuse:this._openData[i].i,
                nums:this._openData[i].zq,
                color:color[i%2]
            };
            baseItem.getComponent(baseItem.name).init(data);
            this.baseItemArr1.push(baseItem);
            
            
        }


         //统计出现期数、最大、平均遗漏、最大连出
        var censusStr = ["出现次数","平均遗漏","最大遗漏","最大连出"];
        var censtrCor1 = new cc.Color(122,30,150);
        var censtrCor2 = new cc.Color(37,87,0);
        var censtrCor3 = new cc.Color(110,35,0);
        var censtrCor4 = new cc.Color(0,96,132);
        var censtrCor5 = new cc.Color(229,225,214);
        var censtrCor6 = new cc.Color(234,233,229);
        var colorBg = [censtrCor5,censtrCor6];
        var colorStr = [censtrCor1,censtrCor2,censtrCor3,censtrCor4];

         var bnData = tempinfo["bn"];
        var baData = tempinfo["ba"];
        var bmData = tempinfo["bm"];
        var bsData = tempinfo["bs"];
        var bnamsList = [];
        bnamsList.push(bnData);
        bnamsList.push(baData);
        bnamsList.push(bmData);
        bnamsList.push(bsData);

        var snData = tempinfo["sn"];
        var saData = tempinfo["sa"];
        var smData = tempinfo["sm"];
        var ssData = tempinfo["ss"];
        var snamsList = [];
        snamsList.push(snData);
        snamsList.push(saData);
        snamsList.push(smData);
        snamsList.push(ssData);


        for(var i=0;i<4;i++)
        {
            //走势统计
            var base1Item = cc.instantiate(this.pfBase1Item);
            var data = {
                color:color[i%2],
                dec:censusStr[i],
                nums:bnamsList[i],
                numColor:colorStr[i]
            }
            base1Item.getComponent(base1Item.name).init(data);
            this.base1ItemArr.push(base1Item);
            
                 //走势统计
            var base1Item = cc.instantiate(this.pfBase1Item);
            var data = {
                color:color[i%2],
                dec:censusStr[i],
                nums:snamsList[i],
                numColor:colorStr[i]
            }
            base1Item.getComponent(base1Item.name).init(data);
            this.base1ItemArr1.push(base1Item);
        }
    },

    init:function(data){
        this._data = data;
    },

    //开奖期号排序
    issueSort:function(toggle){
        this.ndOpenPanle.parent.parent.getComponent(cc.ScrollView).scrollToTop(0.1);
        if(toggle.getComponent(cc.Toggle).isChecked){
            this._openData =this._openData.reverse(); 
        }
        else{
            this._openData =this._openData.reverse();
        }
       
        for(var i=0;i<this._openItemArr.length;++i){
            this._openItemArr[i].setPosition(0,- this._openItemArr[i].height*(i)-this._spacing*(i+1));
            var data = {
                Isuse:this._openData[i].i,
                openNums:this._openData[i].n,
                sum:this._openData[i].s,
                span:this._openData[i].sp,
                repeat:this._openData[i].d,
                color:this._color[i%2],
                type:2
            };
            this._openItemArr[i].getComponent(this._openItemArr[i].name).onItemIndex(i);
            this._openItemArr[i].getComponent(this._openItemArr[i].name).updateData(data); 
        }
    },

    scrollCallBackFun: function(){
        if(!this._openItemArr.length){
            return;
        }else{
            Utils.preNodeComplex(this.ndOpenPanle,this.openItemHeight,this._spacing,this._reaCount,this._openItemArr,this.buffer,this._openData,this._color,'115_opencontent_item',2); 
 
        }
    },

    //----切换到那个单选按钮，才将它们addChild到舞台上来
    //万位走势
    tgWanTrend: function(){
        if(this._addType){return}
        for(var i=0;i<this.baseItemArr.length;++i){
            this.ndWanPanle.addChild(this.baseItemArr[i]);  
            this._addType =true; 
        }
        for(var i=0;i<this.base1ItemArr.length;++i){
            this.ndWanPanle.addChild(this.base1ItemArr[i]);
            this._addType =true;    
        }    
    },

    //千位走势
    toQianTrend: function(){
        if(this._addType1){return}
        for(var i=0;i<this.baseItemArr1.length;++i){
            this.ndQianPanle.addChild(this.baseItemArr1[i]);  
            this._addType1 =true; 
        }
        for(var i=0;i<this.base1ItemArr1.length;++i){
            this.ndQianPanle.addChild(this.base1ItemArr1[i]);
            this._addType1 =true;    
        }    
    }

});
