/*
和值
*/
cc.Class({
    extends: cc.Component,

    properties: {
        k3ToggleList:{
            default:[],
            type:cc.Toggle
        },

        k3DXDS:{
            default:[],
            type:cc.Toggle
        },  
        
        labSelectedAmount:{
            default:null,
            type:cc.Label
        },
        edMutipleNum:{
            default:null,
            type:cc.EditBox
        },
        edIssueNum:{
            default:null,
            type:cc.EditBox
        },

        labTip:{
            default:null,
            type:cc.Label
        },

        ndBetPanle:{
            default: null,
            type: cc.Node
        },
      //机选清空切换按钮
        tgSelect:{
            default:null,
            type:cc.Toggle
        },

        tgIsStop:{
            default:null,
            type:cc.Toggle
        },

        ndAddAward:{
            default:null,
            type:cc.Node
        },

        BASEMONEY:2,
        _rules:"",//规则
        _lotteryID:0,//彩种id
        _curMoney:0,
        _curBetNum:0,
        _totalisuse:0,
        _isStops:-1,//追号到截止
        _selDiceNums:[],
        _betManage:null,
    },

    initReset:function(){
        if(this.isMiss)
            this._betManage.showK3Miss(this.k3ToggleList,this._lotteryID,this._lotteryID + this._rules.toString(),false);
    },

    //退出界面时重置界面
    clearAllBetRecord:function(){
       for(var i = 0;i<this.k3DXDS.length;i++ )
        {
            this.k3DXDS[i].getComponent(cc.Toggle).isChecked = false;
        }
        this.clearAllBetSel();
    },

    // use this for initialization
    onLoad: function () {  
          this.node.on(cc.Node.EventType.TOUCH_END, function (event) {
                var contentRect = this.ndBetPanle.getBoundingBoxToWorld();
                var touchLocation = event.getLocation();
                contentRect.x = contentRect.x -20;
                contentRect.width = 1080;
                if(cc.rectContainsPoint(contentRect, touchLocation) == false){//关闭投注界面
                    this.onClose();
                }
        }, this);

        this._totalisuse = 87;//一天期数
        this.initPanel();
        this._betManage = cc.find("Canvas").getChildByName("ChatRoomPage").getChildByName("BetManage").getComponent("BetManage");
    },

    showAddAward:function(data){
        for(var i=0;i<data.length;i++)
        {
            var codestr = data[i].PlayCode.toString();
            var code = codestr.substring(codestr.length-2,codestr.length);
            var index = this._rules.indexOf(code); 
            if(index != -1)
            {
                this.ndAddAward.active = true;
                return;
            }
        }
    },

    initPanel:function(){
        for(var i=0;i<this.k3ToggleList.length;i++)
        {
            var checkEventHandler = new cc.Component.EventHandler();
            checkEventHandler.target = this.node; 
            checkEventHandler.component = "K3SumCenterContent"
            checkEventHandler.handler = "onClickCallBack";
            checkEventHandler.customEventData = i+4;
            this.k3ToggleList[i].getComponent(cc.Toggle).checkEvents.push(checkEventHandler);
        }

        for(var i=0;i<this.k3DXDS.length;i++)
        {
            var checkEventHandler = new cc.Component.EventHandler();
            checkEventHandler.target = this.node; 
            checkEventHandler.component = "K3SumCenterContent"
            checkEventHandler.handler = "onSelOtherBack";
            checkEventHandler.customEventData = i;
            this.k3DXDS[i].getComponent(cc.Toggle).checkEvents.push(checkEventHandler);

        }
    },

    onSelOtherBack:function(toggle, customEventData){
        this.clearAllBetSel();

        if(toggle.getComponent(cc.Toggle).isChecked)
        {
            if(customEventData == 0)//大
            {
                this.k3DXDS[1].getComponent(cc.Toggle).isChecked = false;
            }
            else if(customEventData == 1)//小
            { 
                this.k3DXDS[0].getComponent(cc.Toggle).isChecked = false;
            }
            if(customEventData == 2)//单
            {
                this.k3DXDS[3].getComponent(cc.Toggle).isChecked = false;
            }
            else if(customEventData == 3)//双
            {
                this.k3DXDS[2].getComponent(cc.Toggle).isChecked = false;
            }
        }
  
        var nums = [];
        var t1 = this.k3DXDS[0].getComponent(cc.Toggle).isChecked;
        var t2 = this.k3DXDS[1].getComponent(cc.Toggle).isChecked;
        var t3 = this.k3DXDS[2].getComponent(cc.Toggle).isChecked;
        var t4 = this.k3DXDS[3].getComponent(cc.Toggle).isChecked;

        if(t1)
        {
            if(t1 && t3)//大单
            {
                nums = [11,13,15,17];
            }
            else if(t1 && t4)//大双
            {
                nums = [12,14,16];//[12,14,16,18];
            }
            else//大
            {
                nums = [11,12,13,14,15,16,17];//[11,12,13,14,15,16,17,18];
            }
        }
        else if(t2)
        {
            if(t2 && t3)//小单
            {
                nums = [5,7,9];//[3,5,7,9];
            }
            else if(t2 && t4)//小双
            {
                nums = [4,6,8,10];
            }
            else//小
            {
                nums = [4,5,6,7,8,9,10];//[3,4,5,6,7,8,9,10];
            }
        }
        else if(t3)//单
        {
            nums = [5,7,9,11,13,15,17];//[3,5,7,9,11,13,15,17];
        }
        else if(t4)//双
        {
            nums = [4,6,8,10,12,14,16];//[4,6,8,10,12,14,16,18];
        }

        for(var i=0;i<nums.length;i++)
        {
            this.k3ToggleList[nums[i]-4].getComponent(cc.Toggle).isChecked = true;
            var temp = this.k3ToggleList[nums[i]-4].checkEvents[0].customEventData;
            this._selDiceNums.push(temp);
        }
        this.updateTgSelect();
        this.checkBet();
    },

    onClickCallBack:function(toggle, customEventData){
        for(var i = 0;i<this.k3DXDS.length;i++ )
        {
            this.k3DXDS[i].getComponent(cc.Toggle).isChecked = false;
        }

        var num =  customEventData;
        if(toggle.getComponent(cc.Toggle).isChecked)
        {
            this._selDiceNums.push(num);
        }
        else
        {
            Utils.removeByValue(this._selDiceNums,num);
        }
       
        this.updateTgSelect();
        this.checkBet();
    },

    //初始化
    init: function(lotteryId){
        this._lotteryID = lotteryId;
        this._isStops = -1;
        this._rules = DEFINE.LOTTERYRULEK3.ANDVALUE;
    },

    //是否中奖后停止追号
    onIsStops:function(toggle, customEventData){
        if(toggle.isChecked == true)
        {
            this._isStops = 0;
        }
        else
        {
            this._isStops = -1;
        }
    },

        //选择清空切换
    onSelectCallBack:function(toggle){
        if(toggle.getComponent(cc.Toggle).isChecked)
        {
            this.randomSelectCallBack();
        }
        else
        {
            this.clearAllBetSel();
        }
    },

    updateTgSelect:function(){
        if(this._selDiceNums.length>0)
        {
            this.tgSelect.getComponent(cc.Toggle).isChecked = true;
        }
        else
        {  
            this.tgSelect.getComponent(cc.Toggle).isChecked = false;
        } 
    },

    //随机选择
    randomSelectCallBack:function(){
        this.clearAllBetSel();
        var randomRedArray = Utils.getRandomArrayWithArray(this.k3ToggleList, 1);
        randomRedArray[0].getComponent(cc.Toggle).isChecked = true;
        var temp = randomRedArray[0].checkEvents[0].customEventData;
        this.onClickCallBack(randomRedArray[0],temp);
        return true;
    },

    //计算
    checkBet:function(){
        var bet = 0;
        bet = this._selDiceNums.length;
        var muiple = this.getMutipleAmount();
        this.setShowAmount(muiple,bet);
    },

    //清除所有选择
    clearAllBetSel:function(){
        this._selDiceNums.length = 0;
        for(var i=0;i<this.k3ToggleList.length;i++)
        {
            this.k3ToggleList[i].getComponent(cc.Toggle).isChecked = false;
        }

        this._isStops = -1;
        this.tgIsStop.getComponent(cc.Toggle).isChecked = false;
        this.tgSelect.getComponent(cc.Toggle).isChecked = false;
        this.setIssueNum("1");
        this.setMutipleAmount("1");
        this.checkBet();
    },


    //设置金额
    setMoney:function(mon){
        this._curMoney = mon;
    },

    //得到金额
    getMoney:function(){
        return this._curMoney;
    },

    //设置注数
    setBetNum:function(num){
        this._curBetNum = num;
    },

    //得到注数
    getBetNum:function(){
        return this._curBetNum;
    },

    //设置期数
    setIssueNum:function(num){
        if(this._totalisuse >= parseInt(num))
        {
            this.edIssueNum.string = num;
        }
        else
        {
            ComponentsUtils.showTips("最大只能选择87期");
            this.edIssueNum.string = this._totalisuse.toString();
        }
        this.checkBet();
    },

    //得到期数
    getIssueNum:function(){
        var num = parseInt(this.edIssueNum.string);
        if(isNaN(num)){ 
            return 1;
        }else{
            return num;
        }        
    },

       //手动期数
    onEditBoxIssueChanged:function(editbox) {
        if(!Utils.isInt(editbox.string))
        {
            ComponentsUtils.showTips("输入格式错误！");
            editbox.string = "1";
        }
        if(editbox.string == null || editbox.string == "")
        {
            editbox.string = "1";
            ComponentsUtils.showTips("倍数不能为空！");
        }

        var amount = parseInt(editbox.string);
        if(isNaN(amount) || amount > 999 || amount<=0 ){
            editbox.string = "1";
        }
  
        this.checkBet();
    },

    //设置倍数
    setMutipleAmount:function(mutiple){
         this.edMutipleNum.string = mutiple;
         this.checkBet();
    },

    //获取当前倍数
    getMutipleAmount:function(){
        var amount = parseInt(this.edMutipleNum.string);
        if(isNaN(amount)){ 
            return 1;
        }else{
            return amount;
        }        
    },

   //手动倍数
    onEditBoxMutipleChanged:function(editbox) {
        if(!Utils.isInt(editbox.string))
        {
            ComponentsUtils.showTips("输入格式错误！");
            editbox.string = "1";
        }
        if(editbox.string == null || editbox.string == "")
        {
            editbox.string = "1";
            ComponentsUtils.showTips("倍数不能为空！");
        }

        var amount = parseInt(editbox.string);
      if(isNaN(amount) || amount<=0 ){
            editbox.string = "1";
        }
  
        this.checkBet();
    },

    //显示投注信息
    setShowAmount:function(mut,bet){
        var issue = this.getIssueNum();
        var money = mut*bet*this.BASEMONEY*issue;
        this.setMoney(money);
        this.setBetNum(bet);
        this.labSelectedAmount.string = "共"+bet+"注"+ mut +"倍"+ issue + "期"+ money+"元";
    },

    //投注信息组合
    dataToJson:function(){
        var objs = [];
        var obj1 = new Object(); 
        var arry1 = [];
        obj1.PlayCode = parseInt(this._lotteryID + this._rules); 
        var nums = "";
        var num = "";
        var dice1 =  this._selDiceNums;
        Utils.sortNum(dice1);
        for(var i = 0;i<dice1.length;i++)
        {
            if(num != "")
            {
                num+=",";
            }  
            num += dice1[i].toString();
        }
        nums = num;

        var numArrays = {
            "Multiple":this.getMutipleAmount(),
            "Bet":this.getBetNum(),
            "isNorm":1,
            "Number": nums
        };
        arry1.push(numArrays);

        obj1.Data = arry1.length >0 ? arry1 : null;
         
        if(obj1.Data != null )
        {
            JSON.stringify(obj1);
            objs.push(obj1);
        }
        var json = JSON.stringify(objs);
        cc.log("提交订单：" + json);
        return json;
    },

    //追号组合支付
    chasePay:function(){
        var recv = function(ret){
            ComponentsUtils.unblock(); 
            if(ret.Code === 0)
            {
                var len = ret.Data.length;
                if(len != this.getIssueNum())
                {
                    this.setIssueNum(len);
                    this.setShowAmount();
                }
                var obj = new Object(); 
                obj.Stops = this._isStops;
                obj.IsuseCount = len;
                obj.BeginTime = ret.Data[0].BeginTime;
                obj.EndTime = ret.Data[len-1].EndTime;
                var arry = [];
                for(var i=0;i<ret.Data.length;i++)
                {
                    var numArrays = {
                        "IsuseID":ret.Data[i].IsuseCode,
                        "Amount": LotteryUtils.moneytoClient(this.getMoney()/this.getIssueNum()),//每期金额
                        "Multiple":this.getMutipleAmount(),//每期总倍数
                    };
                    arry.push(numArrays);
                }
                obj.Data = arry;
                var json = JSON.stringify(obj);

                var data = {
                    lotteryId:this._lotteryID,//彩种id
                    dataBase:this.dataToJson(),//投注信息
                    otherBase:json,//追号
                    money:this.getMoney(), 
                    buyType: this.getIssueNum() >1?1:0,//追号
                }

                window.Notification.emit("BET_ONPAY",data);
            }
            else
            {
                ComponentsUtils.showTips(ret.Msg);
            }
        }.bind(this);
        var data = {
            Token:User.getLoginToken(),
            LotteryCode:this._lotteryID,
            Top:this.getIssueNum(),
        }
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.GETADDTOISUSE, data, recv.bind(this),"POST");   
        ComponentsUtils.block();    
    },

   //关闭界面
    onClose:function(){
        window.Notification.emit("BET_ONCLOSE","");
    },



    //付款
    onPayBtn:function(){
        if(this.getMoney() <= 0)
            return;

        if(this.getIssueNum()<=1)
        {
            var data = {
                lotteryId:this._lotteryID,//彩种id
                dataBase:this.dataToJson(),//投注信息
                otherBase:"",//追加
                money:this.getMoney(),
                buyType: 0,//追号
            }
            window.Notification.emit("BET_ONPAY",data);
        }
        else
        {
            this.chasePay();
        }
    },

    onNextPage:function(){
        window.Notification.emit("BET_NEXTPAGE",1);
    },

    onLastPage:function(){
        window.Notification.emit("BET_NEXTPAGE",-1);
    },

    onMiss:function(toggle){
        if(toggle.getComponent(cc.Toggle).isChecked)
        {
            this.isMiss = true;
            this._betManage.showK3Miss(this.k3ToggleList,this._lotteryID,this._lotteryID + this._rules.toString(),false);
        }    
        else
        {
            this.isMiss = false;
            this._betManage.setK3Miss(false,this.k3ToggleList,"");
        }
    },

});
