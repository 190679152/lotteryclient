/**
 * 公共界面类
 * 包含：弹出框、等待界面
 */

//开启抗锯齿
cc.view.enableAntiAlias(true);
(function(){ 
    var defaultSpriteFrame = null;
    var tipPrefab = null;
    var tips_loading_bg = null;
    var tips_loading_icon = null;
    
    var tips_doubleBtn_noTitle_prefab = null;
    var tips_doubleBtn_prefab = null;
    var tips_singleBtn_noTitle_prefab = null;
    var tips_singleBtn_prefab = null;
    var tips_noButton_noTitle = null;

    //var ROOT_PAGE_TAG = 0;//主界面固定为0
    var PANEL_TAG = 1000;//普通框
    var TIP_TAG = 40000;//提示
    var TIP_LOGIN_TAG = 400001;//等待提示
    
    //等待提示
    var blockNode = null;
    var autoUnblock = true; 
    var timeoutID = 0; 

    function _showTips(content){
        if(content === ""){
            return;
        }
        var node = new cc.Node();    
        var canvas = cc.find("Canvas");
        canvas.addChild(node, TIP_TAG);
        
        var nodeForLabel = new cc.Node();
        var label = nodeForLabel.addComponent(cc.Label);
        label.string = content;
        nodeForLabel.color = cc.Color.WHITE;
        label.fontSize = 56;
        label.lineHeight = 60;
        node.addChild(nodeForLabel,2);
        
        var nodeForBg = new cc.Node();
        var bg = cc.instantiate(tipPrefab);
        nodeForBg.color = cc.Color.BLACK;
        nodeForBg.addChild(bg);
        node.addChild(nodeForBg,1);
        bg.width = label.node.width+20;
        node.y = 0;
        node.height = 160;

        var callfun = cc.callFunc(function(){
            node.destroy();
        });

        node.runAction(cc.sequence(cc.delayTime(1),cc.fadeOut(2), callfun));
    }
    
    function _showTipsByNode(content){
        var node = new cc.Node();
        
        var canvas = cc.find("Canvas");
        canvas.addChild(node);
        
        var nodeForLabel = new cc.Node();
        var label = nodeForLabel.addComponent(cc.Label);
        label.string = content;
        nodeForLabel.color = cc.Color.BLACK;
        label.fontSize = 30;
        node.addChild(nodeForLabel,2);
        
        var nodeForBg = new cc.Node();
        var bg = cc.instantiate(tipPrefab);
        
        nodeForBg.color = cc.Color.BLACK;
        
        nodeForBg.addChild(bg);

        bg.type = cc.Sprite.Type.SLICED;
        node.addChild(nodeForBg,1);
        bg.width = label.node.width+20;
       
        node.y = 0;
        node.height = 80;
        var callfun = cc.callFunc(function(){
            node.destroy();
        });
        node.runAction(cc.sequence(cc.fadeOut(2), callfun));
       
    }
    
    //保证有且只有一个
    function _showLoadingTips(){
        //cc.log("http block");
        if(blockNode !== null && blockNode !== NaN)
        {
            _unblock();
        }

        var node = new cc.Node();
        var canvas = cc.find("Canvas");
        if(canvas == null){
            return null;   
        }
        canvas.addChild(node, TIP_LOGIN_TAG);
        var screenSize = cc.view.getVisibleSize();
        
        node.width = screenSize.width;
        node.height = screenSize.height;
        
        var nodeForBg = new cc.Node();
        var sprite = nodeForBg.addComponent(cc.Sprite);

        sprite.spriteFrame = defaultSpriteFrame;
        nodeForBg.color = cc.Color.BLACK;
        
        node.addChild(nodeForBg);
        
        nodeForBg.width = screenSize.width;
        nodeForBg.height = screenSize.height;
        nodeForBg.opacity = 0;

        var loadingIconBgNode = new cc.Node();
        var loadingIconBgSprite = loadingIconBgNode.addComponent(cc.Sprite);
        loadingIconBgSprite.spriteFrame = tips_loading_bg;
        node.addChild(loadingIconBgNode);      
        var loadingIconNode = new cc.Node();
        var loadingIconSprite = loadingIconNode.addComponent(cc.Sprite);
        loadingIconSprite.spriteFrame = tips_loading_icon;
        
        loadingIconNode.runAction(cc.repeatForever(cc.rotateTo(2, -720)));
        loadingIconBgNode.addChild(loadingIconNode);
        
        node.on(cc.Node.EventType.MOUSE_DOWN, function (event) {

        }, this);
        node.on(cc.Node.EventType.TOUCH_MOVE, function (event) {
            
        }, this);
        
        blockNode = node;
        autoUnblock = true;

        timeoutID = setTimeout(function(){
            if(autoUnblock)
            {
                _unblock();
            }    
        },10000);
    }

    function _unblock(){
        //cc.log("http ubblock");
        if(blockNode !== null && blockNode !== NaN)
        {
            autoUnblock = false;
            clearTimeout(timeoutID);
            blockNode.destroy();
            blockNode = null;
        }   
    }

    /**
     * prams
     * type：1有两个按钮无标题，2有两个按钮与标题，3有一个按钮无标题，4有一个按钮有标题, 5无按钮无标题
     */ 
    function _showAlertTips(type, contentStr, closeCallBack, titleStr, confirmCallback, confirmBtnStr, cancelBtnStr){
        //cc.log("_showAlertTips,content:"+contentStr+",title:"+titleStr);
        var tipsNode = null;
        
        if(type == 1){
            tipsNode = cc.instantiate(tips_doubleBtn_noTitle_prefab);
        }else if(type == 2){
            tipsNode = cc.instantiate(tips_doubleBtn_prefab);
        }else if(type == 3){
            tipsNode = cc.instantiate(tips_singleBtn_noTitle_prefab);
        }else if(type == 4){
            tipsNode = cc.instantiate(tips_singleBtn_prefab);
        }else if(type == 5){
            tipsNode = cc.instantiate(tips_noButton_noTitle);
        }    

        var canvas = cc.find("Canvas");
        canvas.addChild(tipsNode);

        var bg = tipsNode.getChildByName("spBg");
        bg.on(cc.Node.EventType.MOUSE_DOWN, function (event) {
           
        }, this);
        bg.on(cc.Node.EventType.TOUCH_MOVE, function (event) {
            
        }, this);
        
        var _closeCallback = function(){
            if(closeCallBack != null ){
                cc.log("componentsutils closeCallBack")
                closeCallBack();
            }
            cc.log("componentsutils backAndRemove")
            tipsNode.getComponent("Page").backAndRemove();
        }.bind(this);
        var _confirmCallback = function(){
            if(confirmCallback != null){
                confirmCallback();
            }
            tipsNode.getComponent("Page").backAndRemove();
        }.bind(this);

        var content = tipsNode.getChildByName("content");
        var contentLabelNode = content.getChildByName("labContent");
        if(type == 1){
            content.getChildByName("btnLeft").on("click", _closeCallback, this);
            content.getChildByName("btnRight").on("click", _confirmCallback, this);
            contentLabelNode.getComponent(cc.Label).string = contentStr.toString();
            if(cancelBtnStr != null){
                content.getChildByName("btnLeft").getChildByName("labDec").getComponent(cc.Label).string = cancelBtnStr;
            }
            if(confirmBtnStr != null){
                content.getChildByName("btnRight").getChildByName("labDec").getComponent(cc.Label).string = confirmBtnStr;
            }
        }else if(type == 2){
            content.getChildByName("btnLeft").on("click", _closeCallback, this);
            content.getChildByName("btnRight").on("click", _confirmCallback, this);
            contentLabelNode.getComponent(cc.Label).string = contentStr.toString();
            content.getChildByName("labDec").getComponent(cc.Label).string = titleStr.toString();
            if(cancelBtnStr != null){
                content.getChildByName("btnLeft").getChildByName("labDec").getComponent(cc.Label).string = cancelBtnStr;
            }
            if(confirmBtnStr != null){
                content.getChildByName("btnRight").getChildByName("labDec").getComponent(cc.Label).string = confirmBtnStr;
            }
        }else if(type == 3){
            content.getChildByName("btn").on("click", _confirmCallback, this);
            contentLabelNode.getComponent(cc.Label).string = contentStr.toString();

            if(confirmBtnStr != null){
                content.getChildByName("btnRight").getChildByName("labDec").getComponent(cc.Label).string = confirmBtnStr;
            }
        }else if(type == 4){
            content.getChildByName("btn").on("click", _confirmCallback, this);
            contentLabelNode.getComponent(cc.Label).string = contentStr.toString();
            content.getChildByName("labDec").getComponent(cc.Label).string = titleStr.toString();
            if(confirmBtnStr != null){
                content.getChildByName("btn").getChildByName("labDec").getComponent(cc.Label).string = confirmBtnStr;
            }
        }else if(type == 5){
            contentLabelNode.getComponent(cc.Label).string = contentStr.toString();
        }
        
        return tipsNode;
    }
    
    function _preloadRes(){
        cc.log("预加载1");
        cc.loader.loadRes("textures/public/pubiic_9white_bg", cc.SpriteFrame, function (err, frame) {
            defaultSpriteFrame = frame;
        });

        cc.loader.loadRes("prefabs/public/public_tips_bg", function (err, prefab) {
            tipPrefab = prefab;
        });   
        cc.loader.loadRes("textures/public/public_loading_icon", cc.SpriteFrame, function (err, frame) {
            tips_loading_icon = frame;
        });

        cc.loader.loadRes("textures/popup/pop_black_bg", cc.SpriteFrame, function (err, frame) {
            tips_loading_bg = frame;
        });

        //弹出预加载
        cc.loader.loadRes("prefabs/public/public_tips_doubleBtn_noTitle", function (err, prefab) {
            tips_doubleBtn_noTitle_prefab = prefab;
        });
        cc.loader.loadRes("prefabs/public/public_tips_doubleBtn", function (err, prefab) {
            tips_doubleBtn_prefab = prefab;
        });
        cc.loader.loadRes("prefabs/public/public_tips_singleBtn_noTitle", function (err, prefab) {
            tips_singleBtn_noTitle_prefab = prefab;
        });
        cc.loader.loadRes("prefabs/public/public_tips_singleBtn", function (err, prefab) {
            tips_singleBtn_prefab = prefab;
        });
        cc.loader.loadRes("prefabs/public/public_tips_noButton_noTitle", function (err, prefab) {
            tips_noButton_noTitle = prefab;
        });
    }

    function _getPanelTag(){
        PANEL_TAG = PANEL_TAG+1;
        return PANEL_TAG;
    }

    function _getBetPanelTag(){
        BET_PANEL_TAG = BET_PANEL_TAG+1;
        return BET_PANEL_TAG;
    }
    
    var _ComponentsUtils = {
        showTips:_showTips,
        preloadRes:_preloadRes,
        showTipsByNode:_showTipsByNode,
        showAlertTips:_showAlertTips,
        getPanelTag:_getPanelTag,
        getBetPanelTag:_getBetPanelTag,
        block:_showLoadingTips,
        unblock:_unblock,
    };
    window.ComponentsUtils = _ComponentsUtils;
} 
)();
